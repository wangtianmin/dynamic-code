package org.tianmin.idea.dcc.service;

/**
 * @Author wangtianmin
 * @Date 2023/8/23 09:34
 * @Description: TODO
 * @Version 1.0
 */
public interface DemoService {
    String demoServiceTestMethod(String word);
}
