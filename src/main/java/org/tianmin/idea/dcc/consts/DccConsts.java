package org.tianmin.idea.dcc.consts;

/**
 * @Author wangtianmin
 * @Date 2023/8/23 10:55
 * @Description: TODO
 * @Version 1.0
 */
public class DccConsts {
    public static final String REDIS_PERFIX_BEFORE = "dcc:code:before:";
    //dcc:code:before:DemoServiceImpl.demoServiceTestMethod(java.lang.String)
    public static final String REDIS_PERFIX_RETURN = "dcc:code:return:";
    //dcc:code:return:DemoServiceImpl.demoServiceTestMethod(java.lang.String)
}
